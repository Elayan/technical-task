# ZenHR Solutions

Multi Level Technical Task - Software Engineers Applicants


## Acknowledgments

* Please refer to position you are applying for.
    - [Fresh & Junior Software Engineer Level](../master/junior-and-fresh-level/)
    - [Mid Software Engineer Level](../master/mid-level/)
    - [Senior Software Engineer Level](../master/senior-level/)
* The expected delivery date will be based on position applied to & features built with application.
* Never miss the chance to review the resources mentioned below.
* Technical Task should be built with **_Ruby On Rails_**.
* We would recommend you to **_avoid_** using Windows Operating System, **Preferred**:
    - Linux Ubuntu
    - Linux through VM
    - Mac OS
    - Cloud IDE
* Please keep your code well-structured, Check our style guide for your reference
    - https://github.com/BoundlessDrop/style-guide
* Each commit is a story of your process, Please be aware of your commit history.

## Project Delivery
- Source Code Delivery:
    - Create a private repository on bitbucket and invite devteam@boundlessdrop.com
- Deploy your app on [Heroku](https://www.heroku.com/) and send us your application link.

## Useful Resources
- https://edgeguides.rubyonrails.org/index.html
- http://edgeguides.rubyonrails.org/api_app.html
- https://github.com/plataformatec/devise
- https://github.com/lynndylanhurley/devise_token_auth
- http://www.justinweiss.com/articles/search-and-filter-rails-models-without-bloating-your-controller/
- https://scotch.io/tutorials/build-a-restful-json-api-with-rails-5-part-one
- https://github.com/mailgun/mailgun-ruby
- https://github.com/aws/aws-sdk-ruby
- https://github.com/ankane/searchkick
- https://github.com/CanCanCommunity/cancancan
