# ZenHR Solutions

Senior Level Technical Task - Software Engineers Applicants

## Required Task
Your mission is to build a simple **_Job Board_** web application based on **_Ruby On Rails_**.

## Project Description
The Job Board will be used by 2 types of users:

- Job Seekers:
Every Job Seeker will be able to:
    * List all active job posts.
    * Apply to job posts.
    * Each user will have his/her own job applications and can be checked if the job application is seen by admin or not.

- Admin User (Could be more than one admin)
The admin for the job board will be able to:
    * Manage Job Posts.
    * List Job Applications that submitted by job seekers.
    * List All Job Posts.

## Resource Permission
* Job Seekers are not authorized to manage the Job.
* Admin Users are not authorized to destroy job applications and users.
* You can use either the cancancan or pundit gems to do this

## Project In Details
- Users
    * Registrations should be done with email and password (Job Seekers).
    * There should be Admin account, which will manage Job Posts.
    * User Registration (Job Seekers only).
    * User Login.
- Job Post Data
    * Job Post must have: title, description.
    * Create a new Job Post (Admin).
    * Update or delete existing Job Post (Admin).
    * List all Job Applications (Admin & Job Seeker).
- Job Applications
    * Job seekers apply to any job by creating a Job Application that will have a status of (Not Seen) by default When an Admin User views a Job Application this will change their status to (Seen).

## Additional Information
- Your application _should_ be built with **PostgreSQL** Database.
- Always be aware about validation and errors handling.
- **_Additional required _** features:
    - Allow users to add resumes and apply using it, resumes would be saved on AWS S3.
    - Implement search for jobs by title or creation date.
    - Add expiry date to job posts, so they can be active and users can apply for them or expired which will not be listed.
    - Add Categories, which _one category could have more than one job post_.
    - Write Models Specs.
    - Write RESTFful APIs for your application.
- **_We do love it more & more_** if you:
    - Used HAML over HTML.
    - Used Sass over CSS.
    - Send emails to the user (Job Seeker) once the application is seen (You can use Mailgun or SendGrid for this).
    - Use ElasticSearch to search for keywords in the Job Description.
    - Implement CI using Travis and use Coverall for code coverage and insert their badges into your readme file.
    - Take care more about Rails Best Practices.

## Estimation Time
We believe that it could be finished within **_10 Days_** from the date you receive the task.

    - Estimation time could be extended to 13 days if you applied all the points mentioned under **_We do love it more & more_**

- **_Please don't forget to read the main README file to get a clear point about delivery options and enjoy a list of helpful resources._**
